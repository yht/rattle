######################################################################
# Configuration

APP=rattle
VER=5.0.19

DATE=$(shell date +%Y-%m-%d)
REPOSITORY=repository
RSITELIB=$(shell Rscript -e "cat(installed.packages()['$(APP)','LibPath'])")
FILES=Makefile DESCRIPTION inst man R

######################################################################
# R Package Management

.PHONY: version
version:
	perl -pi -e 's|^Version: .*|Version: $(VER)|' DESCRIPTION
	perl -pi -e 's|^Date: .*|Date: $(DATE)|' DESCRIPTION
	perl -pi -e 's|^VERSION <- .*|VERSION <- "$(VER)"|' R/$(APP).R
	perl -pi -e 's|^DATE <- .*|DATE <- "$(DATE)"|' R/$(APP).R

.PHONY: check
check: clean version build
	/usr/lib/R/bin/R CMD check --as-cran --check-subdirs=yes $(APP)_$(VER).tar.gz

.PHONY: build
build: version $(APP)_$(VER).tar.gz

.PHONY: rebuild
rebuild: build install

.PHONY: install
install: build
	/usr/lib/R/bin/R CMD INSTALL $(APP)_$(VER).tar.gz

.PHONY: test
test: install
	Rscript -e 'library($(APP));$(APP)();Sys.sleep(120)'

$(APP)_$(VER).tar.gz: $(FILES) .Rbuildignore
	/usr/lib/R/bin/R CMD build .

$(APP)_$(VER).check: $(APP)_$(VER).tar.gz
	/usr/lib/R/bin/R CMD check --as-cran --check-subdirs=yes $^

.PHONY: ucheck
ucheck: $(APP)_$(VER).tar.gz
	sh ./upload_uwe.sh
	@echo Wait for email from Uwe Legge.

.PHONY: cran
cran: $(APP)_$(VER).tar.gz
	sh ./upload_cran.sh
	@echo Be sure to email cran@r-project.org.

.PHONY: src
src: $(APP)_$(VER)_src.zip  $(APP)_$(VER)_src.tar.gz

.PHONY: zip
zip: $(APP)_$(VER).zip

$(APP)_$(VER)_src.zip: Makefile $(FILES)
	zip -r $@ $^
		-x *~		 \
		-x */*~          \
		-x .Rbuildignore \
		-x .git/**\* 	 \
		-x .git/      	 \
		-x .gitignore 	 \
		-x .Rhistory

$(APP)_$(VER)_src.tar.gz: Makefile $(FILES)
	tar zcvf $@ $^ \
		--exclude="*~" 		\
		--exclude=".git*" 	\
		--exclude=".R*"

$(APP)_$(VER).zip: install
	(cd $(RSITELIB); zip -r9 - $(APP)) >| $(APP)_$(VER).zip

########################################################################
# Version Control - git

status:
	@echo "-------------------------------------------------------"
	git status --untracked-files=no
	@echo "-------------------------------------------------------"

info:
	git info

pull:
	@echo "-------------------------------------------------------"
	git pull
	@echo "-------------------------------------------------------"

push:
	@echo "-------------------------------------------------------"
	git push
	@echo "-------------------------------------------------------"

diff:
	@echo "-------------------------------------------------------"
	git --no-pager diff --color
	@echo "-------------------------------------------------------"

vdiff:
	git difftool

log:
	@echo "-------------------------------------------------------"
	git --no-pager log --stat --max-count=10
	@echo "-------------------------------------------------------"

fulllog:
	@echo "-------------------------------------------------------"
	git --no-pager log
	@echo "-------------------------------------------------------"

########################################################################
# Source code management and distribution.

.PHONY: dist
dist: home repo version access

.PHONY: home
home:
	(cd /home/gjw/projects/togaware/www/;\
	 perl -pi -e "s|Version [0-9\.]* |Version $(VER) |" \
			rattle.html.in;\
	 perl -pi -e "s|rattle_[0-9\.]*.tar.gz|rattle_$(VER).tar.gz|" \
			rattle.html.in;\
	 perl -pi -e "s|access/rattle_[0-9\.]*_src.tar.gz|access/rattle_$(VER)_src.tar.gz|" \
			rattle.html.in;\
	 perl -pi -e "s| dated [^\.]*\.| dated $(DATE).|" \
			rattle.html.in;\
	 make rattle)

.PHONY: access
access: zip
	chmod 0644 $(APP)_$(VER).tar.gz $(APP)_$(VER).zip
	rsync -ahv $(APP)_$(VER).tar.gz $(APP)_$(VER).zip \
		   togaware.com:webapps/access/

#	chmod 0644 $(APP)_$(VER)_src.zip $(APP)_$(VER)_src.tar.gz \
#		   $(APP)_$(VER).tar.gz $(APP)_$(VER).zip
#	rsync -ahv $(APP)_$(VER)_src.zip $(APP)_$(VER)_src.tar.gz \
#		   $(APP)_$(VER).tar.gz $(APP)_$(VER).zip \
#		   togaware.com:webapps/access/

.PHONY: repo
repo: build zip
	cp $(APP)_$(VER).tar.gz $(APP)_$(VER).zip $(REPOSITORY)
	-R --no-save < repository/repository.R
	chmod go+rx $(REPOSITORY)
	chmod go+r $(REPOSITORY)/*
	rsync -ahv repository/ togaware.com:webapps/rattle/src/contrib
	rsync -ahv repository/*.zip togaware.com:webapps/rattle/bin/windows/contrib/all/
	rsync -ahv repository/PACKAGE* togaware.com:webapps/rattle/bin/windows/contrib/all/
	rsync -ahv inst/NEWS togaware.com:webapps/rattle/NEWS

.PHONY: weather
weather: 
	(cd weather; make all)

########################################################################
# Administration

.PHONY: clean
clean:
	rm -vf *~ .*~ */*~
	rm -rf $(APP).Rcheck
	rm -vf file*.xdf
	rm -vf weather_rattle.xdf xgboost.model

.PHONY: realclean
realclean: clean
	-@mv -vf $(APP)_* BACKUP/
